package com.tw;

public class SmartParkingBoy extends ParkingBoyBase {
    // TODO: You can change anything within this range including which method to override.
    // <-start-
    @Override
    public ParkingTicket park(Car car) {
        ParkingTicket ticket = null;
        int maxPosition = 0;
        for (int j = 0; j < getParkingLots().size(); j++){
            if ( getParkingLots().get(j).getAvailableParkingPosition() >= maxPosition){
                maxPosition = getParkingLots().get(j).getAvailableParkingPosition();
            }
        }
        for (int i = 0; i < getParkingLots().size(); i++){
            try {
                if ( getParkingLots().get(i).getAvailableParkingPosition() == maxPosition){
                    ticket = getParkingLots().get(i).park(car);
                    setLastErrorMessage(null);
                    break;
                }
            } catch (ParkingLotFullException e){
                setLastErrorMessage("The parking lot is full.");
            }
        }
        return ticket;
    }


    @Override
    public Car fetch(ParkingTicket ticket) {
        return super.fetch(ticket);
    }
    // --end->
}
