package com.tw;

public class ParkingBoyFactory {
    public static final int PARKING_BOY = 1;
    public static final int SMART_PARKING_BOY = 2;
    public static final int SUPER_SMART_PARKING_BOY = 3;

    public static ParkingBoy create(int type, ParkingLot... parkingLots) {
        ParkingBoy parkingBoy;
        switch (type) {
            case PARKING_BOY:
                parkingBoy = new NormalParkingBoy();
                break;
            case SMART_PARKING_BOY:
                parkingBoy = new SmartParkingBoy();
                break;
            case SUPER_SMART_PARKING_BOY:
                parkingBoy = new SuperSmartParkingBoy();
                break;
            default:
                throw new IllegalArgumentException(String.format("Unknown type: %d", type));
        }

        if (parkingLots != null && parkingLots.length > 0) {
            parkingBoy.addParkingLot(parkingLots);
        }

        return parkingBoy;
    }
}
