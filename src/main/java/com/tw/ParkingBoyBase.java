package com.tw;

import java.util.*;

public abstract class ParkingBoyBase implements ParkingBoy {
    private final List<ParkingLot> parkingLots = new ArrayList<>();
    private String lastErrorMessage;

    @Override
    public void addParkingLot(ParkingLot... parkingLots) {
        this.parkingLots.addAll(Arrays.asList(parkingLots));
    }

    // TODO: You can override methods or add new methods here if you want
    // <-start-

    @Override
    public Car fetch(ParkingTicket ticket) {
        Car car = null;
        if (ticket == null){
            car = null;
            setLastErrorMessage("Please provide your parking ticket.");
        } else {
            int i = 0;
            while (i < parkingLots.size()) {
                try {
                    car = parkingLots.get(i).fetch(ticket);
                } catch (InvalidParkingTicketException e) {
                    setLastErrorMessage("Unrecognized parking ticket.");
                    i++;
                }
            }
            if (car != null) {
                setLastErrorMessage(null);
            }
        }
        return car;
    }
    // --end->

    @Override
    public String getLastErrorMessage() {
        return lastErrorMessage;
    }

    protected void setLastErrorMessage(String errorMessage) {
        lastErrorMessage = errorMessage;
    }

    protected List<ParkingLot> getParkingLots() {
        return Collections.unmodifiableList(this.parkingLots);
    }
}
