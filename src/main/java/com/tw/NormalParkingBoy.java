package com.tw;

public class NormalParkingBoy extends ParkingBoyBase {
    // TODO: You can change anything within this range including which method to override.
    // <-start-
    @Override
    public ParkingTicket park(Car car) {
        ParkingTicket ticket = null;
        for (int i = 0; i < getParkingLots().size(); i++){
            try {
                ticket = getParkingLots().get(i).park(car);
                setLastErrorMessage(null);
                break;
            } catch (ParkingLotFullException e){
                setLastErrorMessage("The parking lot is full.");
            }
        }
        return ticket;
    }

    @Override
    public Car fetch(ParkingTicket ticket) {
        return super.fetch(ticket);
    }
    // --end->
}
