package com.tw;

public class SuperSmartParkingBoy extends ParkingBoyBase {
    // TODO: You can change anything within this range including which method to override.
    // <-start-
    @Override
    public ParkingTicket park(Car car) {
        ParkingTicket ticket = null;
        double maxRate = 0;
        for (int j = 0; j < getParkingLots().size(); j++){
            if ( (double)getParkingLots().get(j).getAvailableParkingPosition() / getParkingLots().get(j).getCapacity() >= maxRate){
                maxRate = (double)getParkingLots().get(j).getAvailableParkingPosition() / getParkingLots().get(j).getCapacity();
            }
        }
        for (int i = 0; i < getParkingLots().size(); i++){
            try {
                if ( (double)getParkingLots().get(i).getAvailableParkingPosition() / getParkingLots().get(i).getCapacity() == maxRate){
                    ticket = getParkingLots().get(i).park(car);
                    setLastErrorMessage(null);
                    break;
                }
            } catch (ParkingLotFullException e){
                setLastErrorMessage("The parking lot is full.");
            }
        }
        return ticket;
    }
    @Override
    public Car fetch(ParkingTicket ticket) {
        return super.fetch(ticket);
    }
    // --end->
}
